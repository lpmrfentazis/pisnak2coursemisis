from verboselogs import VerboseLogger, SPAM, NOTICE, VERBOSE
from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL
from datetime import datetime
from pathlib import Path

from time import gmtime

import logging
import coloredlogs



class LorettLogger:
    def __init__(self, name: str, path: str, level: int) -> None:
        self.mylogs = VerboseLogger(__name__)
        self.level = level

        self.mylogs.setLevel(self.level)

        self.path = Path(path)

        
        if not self.path.exists():
            self.path.mkdir(parents=True, exist_ok=True)

        fileName = datetime.utcnow().strftime(f"{name}_%d-%m-%Y") + ".log"
        fileName = self.path / fileName

        self.file = logging.FileHandler(fileName)
        self.fileformat = logging.Formatter(
            "%(asctime)s:%(levelname)s:%(message)s")

        logging.Formatter.converter = gmtime

        self.file.setLevel(level)
        self.file.setFormatter(self.fileformat)

        self.stream = logging.StreamHandler()
        self.streamformat = logging.Formatter(
            "%(levelname)s:%(module)s:%(message)s")

        self.stream.setLevel(level)
        self.stream.setFormatter(self.streamformat)

        # инициализация обработчиков
        self.mylogs.addHandler(self.file)
        self.mylogs.addHandler(self.stream)

        coloredlogs.install(level=level, logger=self.mylogs,
                            fmt='%(asctime)s [%(levelname)s] - %(message)s')

        logging.Formatter.converter = gmtime                            

        self.lastLog = {"message": "-",
                        "source": "station"}

        self.mylogs.info('Start Logger')

    def spam(self, message: str, source: str = "station") -> None:
        
        if self.level <= SPAM:
            self.lastLog["message"] = message
            self.lastLog["source"] = source

        self.mylogs.spam(message)
    
    def verbose(self, message: str, source: str = "station") -> None:

        if self.level <= VERBOSE:
            self.lastLog["message"] = message
            self.lastLog["source"] = source

        self.mylogs.verbose(message)

    def notice(self, message: str, source: str = "station") -> None:
        if self.level <= NOTICE:
            self.lastLog["message"] = message
            self.lastLog["source"] = source

        self.mylogs.notice(message)

    def debug(self, message: str, source: str = "station") -> None:

        if self.level <= DEBUG:
            self.lastLog["message"] = message
            self.lastLog["source"] = source
                            
        self.mylogs.debug(message)


    def info(self, message: str, source: str = "station") -> None:
        
        if self.level <= INFO:
            self.lastLog["message"] = message
            self.lastLog["source"] = source

        self.mylogs.info(message)

    def warning(self, message: str, source: str = "station") -> None:
        
        if self.level <= WARNING:
            self.lastLog["message"] = message
            self.lastLog["source"] = source

        self.mylogs.warning(message)

    def critical(self, message: str, source: str = "station") -> None:
        
        if self.level <= CRITICAL:
            self.lastLog["message"] = message
            self.lastLog["source"] = source

        self.mylogs.critical(message)
        exit(-1) 

    def error(self, message: str, source: str = "station") -> None:

        if self.level <= ERROR:
            self.lastLog["message"] = message
            self.lastLog["source"] = source
                        
        self.mylogs.error(message)
