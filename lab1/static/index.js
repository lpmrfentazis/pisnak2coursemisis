var host = window.location.host
document.addEventListener("DOMContentLoaded", readyBase);

var date = new Date(Date.now());


function updateTime(){

    //console.log(date);
    var time = [date.getUTCHours(),date.getUTCMinutes(),date.getUTCSeconds()]; // |[0] = Hours| |[1] = Minutes| |[2] = Seconds|
    //var dayOfWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    //var curDate = date.getUTCDate();
    
    if(time[0] < 10){time[0] = "0"+ time[0];}
    if(time[1] < 10){time[1] = "0"+ time[1];}
    if(time[2] < 10){time[2] = "0"+ time[2];}
    
    var current_time = [time[0],time[1],time[2]].join(':');
    var clock = document.getElementById("clock");
    var dateEl = document.getElementById("date");
    
    clock.textContent = current_time + " UTC";
    
    dateEl.textContent = String(date.getUTCDate()).padStart(2, '0') + "." + String(date.getUTCMonth() + 1).padStart(2, '0') + "." + date.getUTCFullYear();
    
}


function clockTimer(){
    date.setSeconds(date.getSeconds() + 1);
}

Date.prototype.isValid = function () {
    // An invalid date object returns NaN for getTime() and NaN is the only
    // object not strictly equal to itself.
    return this.getTime() === this.getTime();
}; 

function onlyDigits() {
    this.value = this.value.replace(/[^\d\.]/g, "");
    if(this.value.match(/\./g).length > 1) {
        this.value = this.value.substr(0, this.value.lastIndexOf("."));
    }
}


function readyBase() {
    function disconnected() {
        console.log("Disconnected");
    }

    function requestDatetime() {
        var request = new XMLHttpRequest();
        request.open('GET', "/getDatetime");
        request.timeout = 1000;
        request.responseType = 'text'
        
        request.onload = function updateDatetime() {
            
            if (this.status == 200) {
                
                var status = document.getElementById("status");
                
                status.style.backgroundColor = "#ffffff";

                var datetime = request.response;

                var t = new Date(Date.UTC(datetime));
                
                if (t.isValid())
                    date = t; 
            }

            else {
                disconnected();
            }

        }; 

        request.ontimeout = disconnected;

        request.send('');
    }


    setInterval( clockTimer, 1000);
    setInterval( updateTime, 1000);
    setInterval( requestDatetime, 10000);

    if( !(document.querySelector(".onlyDigits") == null) )
        document.querySelector(".onlyDigits").onkeyup = onlyDigits
}
