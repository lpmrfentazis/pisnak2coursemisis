var bases = {
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "A": 10,
    "B": 11,
    "C": 12,
    "D": 13,
    "E": 14,
    "F": 15,
}

function checkConverter() {
    var value1 = document.getElementById("degree").value;
    var value2 = document.getElementById("minutes").value;
    var value2 = document.getElementById("seconds").value;
}

function checkCalculator() {
    var value1 = document.getElementById("variable1").value.toUpperCase();
    var value2 = document.getElementById("variable2").value.toUpperCase();

    var operator = document.getElementById("operator").value;

    var base1 = document.getElementById("base1").value;
    var base2 = document.getElementById("base2").value;

    if ((value1.split(".").length - 1) > 1) {
        alert("There are too many dots in expression 1");
        return;
    }

    if ((value2.split(".").length - 1) > 1) {
        alert("There are too many dots in expression 2");
        return;
    }

    for (var i = 0; i < value1.length; ++i) {
        if (bases[value1[i]] > parseInt(base1)) {
            alert("Invalid characters in 1 expression");
            return;
        }
    }

    for (var i = 0; i < value2.length; ++i) {
        if (bases[value2[i]] > parseInt(base2)) {
            alert("Invalid characters in 2 expression");
            return;
        }
    }



    var request = new XMLHttpRequest();
    request.open('POST', "/calculate");
    request.timeout = 1000;
    request.responseType = "text";
    request.setRequestHeader("Content-Type", "application/json");

    let data = JSON.stringify({"value1": value1, 
                                "value2": value2, 
                                "operator": operator, 
                                "base1": base1, 
                                "base2": base2
                            });
    //console.log(data);
    request.send(data);

    request.onload = function result() {
        if (this.status == 200) {
            alert(request.response);
            
            return true;
        }
        
        alert("Fail");
        return false;
        
    }

}