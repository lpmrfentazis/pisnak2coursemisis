from datetime import datetime, timedelta
from flask import Flask, abort, jsonify, render_template, redirect, request, Response 
from logging import  getLogger
from LorettLogger import LorettLogger, SPAM, VERBOSE, DEBUG, INFO, WARNING

from pathlib import Path
from os import getcwd

from Calculator import Calculator, toBaseFloat

import json




__version__ = "0.1"

config = {
    "name": "testServer",
    "loggingLevel": "info",
    "logPath": "./log",
    "workingPath": ".",
}


# IDK
def parseConfig(conf) -> dict:
    outputConfig = {}
    messages = []

    if isinstance(conf, (str, Path)):
        if Path(conf).exists():
            with open(conf) as file:
                outputConfig = json.load(file)

            if outputConfig.keys() == config.keys():
                messages.append(("info", f"Config loaded from {conf}"))

            else:
                outputConfig = config

                messages.append(("warning", f"Incorrect config in {conf}"))
                messages.append(("info", "Load default config"))

                with open(conf, 'w') as f:
                    f.write(json.dumps(config, indent=1))
        else:
            outputConfig = config

            messages.append(("warning", f"Incorrect config in {conf}"))
            messages.append(("info", "Load default config"))

            with open(conf, 'w') as f:
                f.write(json.dumps(config, indent=4))

    elif isinstance(conf, dict):
        if conf.keys() == config.keys():
            outputConfig = conf
            messages.append(("info", f"Config loaded from {conf}"))

        else:
            outputConfig = config

            messages.append(("warning", f"Incorrect config in {conf}"))
            messages.append(("info", "Load default config"))

    else:
        outputConfig = config

        messages.append(("warning", f"Incorrect config in {conf}"))
        messages.append(("info", "Load default config"))

    return outputConfig, messages


class Server:
    def __init__(self, config: dict, messages: list) -> None:
        if config["workingPath"] == ".":
            config["workingPath"] = getcwd()

        self.name = config["name"]
        self.logPath = Path(config["logPath"])
        self.workingPath = Path(config["workingPath"])

        getLogger('werkzeug').setLevel(WARNING)
        self.logger: LorettLogger = LorettLogger(self.name, self.logPath, level=config["loggingLevel"])

        if len(messages):
            for type, mes in messages:
                if (type == "info"):
                    self.logger.info(mes)

                if (type == "warning"):
                    self.logger.warning(mes)

        self.app = Flask(self.name, 
                         template_folder=self.workingPath / "templates", 
                         static_folder= self.workingPath / "static")
        self.app.config['JSON_AS_ASCII'] = False

        self.addEndPoint('/', 'start', self.start, methods=['GET'])
        self.addEndPoint('/home', 'home', self.index, methods=['GET'])
        self.addEndPoint('/index', 'index', self.index, methods=['GET'])
        self.addEndPoint('/calculator', 'calculator', self.calculator, methods=['GET'])
        
        self.addEndPoint('/calculate', 'calculate', self.calculate, methods=['POST'])
        self.addEndPoint('/example/<int:storeID>', 'getWithParamExample', self.getWithParamExample, methods=['GET'])
        
        self.addEndPoint('/example/post/', 'postExample', self.postExample, methods=['POST'])

        @self.app.before_request
        def logRequest():
            self.logger.debug(f"{request.method} - {request.url}")
            self.logger.spam(f"{request.method} - {request.url} from {request.remote_addr}")

    def start(self) -> Response:
        return redirect("/home")

    def index(self) -> str:
        now = datetime.utcnow()
        return render_template("index.html", name=self.name,
                                             time = now.strftime("%H:%M:%S UTC"),
                                             date = now.strftime("%d.%m.%Y")) 

    def calculator(self) -> str:
        return render_template("calculator.html") 
    
    def calculate(self) -> str:
        if request.method == "POST":
            data = request.json

            value1 = str(toBaseFloat(data["value1"], base=int(data["base1"])))
            value2 = str(toBaseFloat(data["value2"], base=int(data["base2"])))
            
            res = Calculator(value1 + data["operator"] + value2).calculate()
            return str(res)

    
    def getWithParamExample(self, storeID: int) -> str:
        jsonResp = {}

        return jsonify(jsonResp)

    
    def postExample(self) -> str:
        if request.method == "POST":
            data = request.get_json()
            jsonResp = {}

            return jsonify(jsonResp)


    def addEndPoint(self, endpoint=None, endpoint_name=None, handler=None, methods=['GET'], *args, **kwargs) -> None:
        self.app.add_url_rule(endpoint, endpoint_name, handler, methods=methods)


    def run(self, **kwargs):
        self.app.run(**kwargs)



if __name__ == "__main__":
    config, messages = parseConfig(Path(__file__).parents[0] / "config.json")

    level = DEBUG

    if "info" in config['loggingLevel'].lower():
        config['loggingLevel'] = INFO

    elif "debug" in config['loggingLevel'].lower():
        config['loggingLevel'] = DEBUG

    elif "warning" in config['loggingLevel'].lower():
        config['loggingLevel'] = WARNING

    elif "spam" in config['loggingLevel'].lower():
        config['loggingLevel'] = SPAM

    elif "verbose" in config['loggingLevel'].lower():
        config['loggingLevel'] = VERBOSE

    server = Server(config=config, messages=messages)
    server.run(host="0.0.0.0", port=80, debug=True)
