"""
    Калькулятор через обратную польскую запись, через стек
    
    !Унарный минус!
    
"""
from Brackets import check
from dataclasses import dataclass


@dataclass
class Operator:
    char: str
    priority: int
    calc: callable


operators = {
    "(": Operator(char="(", priority=0, calc=None),
    ")": Operator(char=")", priority=0, calc=None),
    "+": Operator(char="+", priority=1, calc=lambda a, b: a + b),
    "-": Operator(char="-", priority=1, calc=lambda a, b: b - a),
    "*": Operator(char="*", priority=2, calc=lambda a, b: a * b),
    "/": Operator(char="/", priority=2, calc=lambda a, b: b / a)
}
alphabet = "0123456789."

def toBaseFloat(string: str, base: int = 10):
    parts = string.split('.')
    if len(parts) > 2:
        raise ValueError("Cannot convert to float")
    
    if len(parts) == 1:
        return int(parts[0], base=base)
    else:
        if not len(parts[0]):
            whole = 0
        else:
            whole = int(parts[0], base=base)
            
        if not len(parts[1]):
            fract = 0
        else:
            fract = int(parts[1], base=base)
            
        
        return whole + fract / 10**len(str(fract))
    
class Calculator:
    __expression: str
    __stack: list
    __elements: list
    
    def __init__(self, expression: str = "") -> None:
        self.__stack = []
        self.__elements = []
        self.expression = expression
    
    @property
    def expression(self) -> str:
        return self.__expression
    
    @expression.setter
    def expression(self, expression: str) -> None:
        if not check(expression):
            raise ValueError("Error in brackets")
        self.__expression = expression
        self.__parse()
        
    def calculate(self) -> float:
        if not len(self.__elements):
            return 0
        
        for element in self.__elements:
            if element not in operators.keys():
                self.__stack.append(element)
            else:
                if len(self.__stack) < 2:
                    raise ValueError("Bad expression")
                
                a = self.__stack.pop()
                b = self.__stack.pop()
                self.__stack.append(operators[element].calc(a, b))
                
        if len(self.__stack) > 1:
            raise ValueError("Bad expression")
        
        return self.__stack.pop()
     
        
    def __convertToRPN(self) -> None:
        rpn = []
        
        for i in self.__elements:
            if i not in operators.keys():
                rpn.append(float(i))
            
            elif len(self.__stack) == 0 or i == "(":
                self.__stack.append(i)
                
            elif i == ")":
                while len(self.__stack):
                    elem = self.__stack.pop()
                    if elem != ")":
                        rpn.append(elem)
                    else:
                        break
                
            elif operators[i].priority <= operators[self.__stack[-1]].priority:
                
                while len(self.__stack):
                    if operators[i].priority <= operators[self.__stack[-1]].priority:
                        rpn.append(self.__stack.pop())
                    else:
                        break
                self.__stack.append(i)
                    
        if len(self.__stack):
            rpn += self.__stack  
        self.__stack = []       
        
        self.__elements = rpn
        
    def __parse(self) -> None:
        elements = []
        
        tmp = ""
        
        for i in self.__expression:
            # ADD check for float
            if i in alphabet:
                tmp += i
                
            elif i in operators.keys():
                if len(elements):
                    if elements[-1] in operators.keys() and tmp == "":
                        raise ValueError("Error in operators")
                
                elements.append(tmp)
                elements.append(i)
                tmp = ""
                
        if len(tmp):
            elements.append(tmp) 
        while len(elements):
            if elements[-1] in operators:
                elements = elements[:-1]
            else:
                break
                
        self.__elements = elements  
        self.__convertToRPN()     
            

if __name__ == "__main__":
    line = "244+2332-3235-8*"
    
    calc = Calculator(line)
    print(calc.calculate())