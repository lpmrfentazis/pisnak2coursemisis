"""
   Проверка правильной скобочной последовательности ({[]}) 
"""

opens = "{[("
closed = "}])"
whiteList = opens+closed


def check(line: str) -> bool:
    stack = []
    for i in line:
        if i not in whiteList:
            continue
        
        if not len(stack) or i in opens:
            stack.append(i)
            continue
        
        tmp = closed.index(i)
        
        if stack[-1] != opens[tmp]:
            return False
        else: stack.pop()
    
    return not len(stack)


if __name__ == "__main__": 
    line = input()
    print(check(line))

        
    
    
    

